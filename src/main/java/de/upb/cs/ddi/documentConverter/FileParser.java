/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.documentConverter;

import de.upb.cs.ddi.tools.pdfconverter.PDFBoxConverterImpl;
import de.upb.cs.ddi.tools.utils.exceptions.CouldNotExtractException;
import de.upb.cs.ddi.tools.utils.exceptions.EmptyPDFException;
import de.upb.cs.ddi.tools.utils.exceptions.TooManyControlCharacters;
import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author lk
 */
public class FileParser {
    
    File file;
    public FileParser(String fileName) {
        file = new File(fileName);
    }
    
    public FileParser(File file) {
        this.file = file;
    }
    
    public DocumentInt ParseFile() throws FileNotFoundException, TooManyControlCharacters, CouldNotExtractException, EmptyPDFException, IOException {
        DocumentInt doc = null;
        boolean ispdf = true;
        InputStream is = new FileInputStream(file);
        try{
            PDFBoxConverterImpl pdfConverter = new PDFBoxConverterImpl(is);
            doc = pdfConverter.getFullText();
        } catch (IOException ex) {
            ispdf = false;
        }
        if(!ispdf) {
            TxtFileParser txtConverter = new TxtFileParser(file);
            doc = txtConverter.getFullText();
        }
        
        return doc;
    }
}
