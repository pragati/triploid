/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.documentConverter;

import de.upb.cs.ddi.tools.utils.Document;
import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author lk
 */
public class TxtFileParser {

    private File file;

    public TxtFileParser(File file) {
        this.file = file;
    }

    public DocumentInt getFullText() throws IOException {
        DocumentInt doc = new Document();
//        BufferedReader br = new BufferedReader(new InputStreamReader(ips));            
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
        String fullText;
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }
        fullText = sb.toString();
        br.close();

        String[] search = {"\u0000", "\u0001", "\u0002", "\u0003", "\u0004", "\u0005", "\u0006", "\u0007", "\u0008", "\u0009", "\u000B", "\u000C", "\u000E", "\u000F", "\u0010", "\u0011", "\u0012", "\u0013", "\u0014", "\u0015", "\u0016", "\u0017", "\u0018", "\u0019", "\u001A", "\u001B", "\u001C", "\u001D", "\u001E", "\u001F"};
        String[] replace = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
        String fulltext_without_control = StringUtils.replaceEach(fullText, search, replace);
        addTextToDoc(doc, fulltext_without_control);
        
        return doc;

    }

    public static void addTextToDoc(DocumentInt doc, String text) throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(text));
        String line = null;
        LinkedList<String> lines = new LinkedList<String>();
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }

        boolean lastcharacterislinefeed = false;
        if (new Character(text.charAt(text.length() - 1)) == '\n') {
            lastcharacterislinefeed = true;
        }

        if (!lastcharacterislinefeed) {
            String last = lines.getLast().replace("\n", "");
            if (last.charAt(last.length() - 1) != ' ' && last.charAt(last.length() - 1) != '-') {
                last = last + " ";
            }
            lines.removeLast();
            lines.add(last);
        }

        Iterator<String> it = lines.iterator();
        while (it.hasNext()) {
            doc.addLine();
            String linestr = it.next();
            if (linestr.length() != 0 && linestr.charAt(linestr.length() - 1) != ' ' && linestr.charAt(linestr.length() - 1) != '-') {
                linestr = linestr + " ";
            }
            doc.addChunk(linestr);
        }
    }
}
