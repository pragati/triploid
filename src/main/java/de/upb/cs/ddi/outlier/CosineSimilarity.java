package de.upb.cs.ddi.outlier;

import java.io.File;
import java.io.FileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularValueDecomposition;

/**
 *
 * @author lk
 */
public class CosineSimilarity {

    
    public CosineSimilarity() {
        
    }
    
    public static void main(String[] args) {

//        Double vec1[] = {7.15, 9.42, 1086.93};
//        Double vec2[] = {1.36, 120.16, 115.97};
//        
//        Double cosSim = CosineSimilarity.similarity(vec1, vec2);
//        System.out.println("Cosine Similarity=" + cosSim);

        File dir = new File("/users/lk/Thesis/2010");
        FileFilter fileFilter = new WildcardFileFilter("*cleaned.txt");
        File[] files = dir.listFiles(fileFilter);
        for (int i = 0; i < files.length; i++) {
           System.out.println(files[i]);
        }
        
        
        
//        double[][] matrixData = { {3d,1d,1d}, {-1d,3d,1d}};
//        RealMatrix m = MatrixUtils.createRealMatrix(matrixData);
//        System.out.println(m.toString());
//        SingularValueDecomposition svd = new SingularValueDecomposition(m);
//        svd.getSolver();
//        double[] sv = svd.getSingularValues();
//        for (int i = 0; i < sv.length; i++) {
//            System.out.println(sv[i]);
//        }
//        System.out.println(svd.getS().toString());
//        System.out.println(svd.getV().toString());
//        System.out.println(svd.getVT().toString());
        
        
    }

    public static Double similarity(Double[] vec1, Double[] vec2) {
        double dp = dotProduct(vec1, vec2);
        double magnitudeA = findMagnitude(vec1);
        double magnitudeB = findMagnitude(vec2);
//        System.out.println("dp = " + dp + " mag = " + magnitudeA*magnitudeB);
        return (dp) / (magnitudeA * magnitudeB);
    }

    private static double findMagnitude(Double[] vec) {
        double sumMag = 0;
        for (int i = 0; i < vec.length; i++) {
            sumMag = sumMag + vec[i] * vec[i];
        }
        return Math.sqrt(sumMag);
    }

    private static double dotProduct(Double[] vec1, Double[] vec2) {
        double sum = 0;
        for (int i = 0; i < vec1.length; i++) {
            sum = sum + vec1[i] * vec2[i];
        }
        return sum;
    }
}
