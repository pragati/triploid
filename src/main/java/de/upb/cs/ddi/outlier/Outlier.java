/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.outlier;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 *
 * @author lk
 */
public class Outlier {
 
    List<Double[]> vectors = new ArrayList<Double[]>();
    List<Double[]> normVectors = new ArrayList<Double[]>();
    RealMatrix docMatrix;
    RealMatrix docMeanMatrix;
    int rows;
    int cols;
    Double[] meanVector;
    Double[] similarity;
    
    public Outlier(List<Double[]> docVectors) {
        rows = docVectors.size();
        Double[] tmp = docVectors.get(0);
        cols = tmp.length;
        docMatrix = MatrixUtils.createRealMatrix(rows, cols);
        for (int i = 0; i < rows; i++) {
            Double[] vector = docVectors.get(i);
            for (int j = 0; j < vector.length; j++) {
//                System.out.print(vector[j] + "\t");
                docMatrix.addToEntry(i, j, vector[j]);
            }
//            System.out.println("");
        }
    }
    
    public void preProcessData() {
//        RealMatrix mat = docMatrix.getSubMatrix(0, 31, 0, 6);
//        docMatrix = mat;
//        rows = 32;
        //mean normalize
        double[] mean = meanNormalize();
        //scale data
        RealMatrix matrix = scaleData(mean);
        PCA pca = new PCA(matrix);
//        cosSimilarity(docMatrix);
//        Double meanSim = 0.0;
//        
//        for(Double sim : similarity) {
//            meanSim += sim;
//        }
//        meanSim = meanSim / rows;
////        System.out.println("mean similarity = " + meanSim);
//        Double sd = calculateSD(meanSim);
//        
////        System.out.println("SD = " + sd);
//        double threshold = meanSim - (0.2 * sd);
////        System.out.println("threshold = " + threshold);
//        for (int i = 0; i < rows; i++) {
//            
//            if(similarity[i] < threshold) {
//                System.out.println("Outlier para" + i);                
//            }
//        }
        
        
    }
    
    public void cosSimilarity(RealMatrix matrix) {
//        int rows = docMatrix.getRowDimension();
//        int cols = docMatrix.getColumnDimension();
        
        RealMatrix normDocMatrix = MatrixUtils.createRealMatrix(rows, cols);
        for (int j = 0; j < rows; j++) {
            RealVector normVector = matrix.getRowVector(j);
            normVector.unitize();
            normDocMatrix.setRowVector(j, normVector);
        }
        
        //mean doc vector
        RealVector normDocVector = normDocMatrix.getRowVector(0);
        for (int j = 1; j < rows; j++) {
            normDocVector = normDocVector.add(normDocMatrix.getRowVector(j));
        }
        
        normDocVector.mapDivideToSelf(cols);
//        Double meanSim = 0.0;
//        System.out.println(normDocVector.toString());
        similarity = new Double[rows];        
        
        for (int j = 0; j < rows; j++) {
            RealVector curVector = normDocMatrix.getRowVector(j);
            double dotProd = curVector.dotProduct(normDocVector);
            double magnitude = curVector.getNorm() * normDocVector.getNorm();
//            System.out.println("dp = " + dotPro + " mag = " + mag);
            similarity[j] = dotProd/magnitude;
//            meanSim += similarity[j];         
//            System.out.println("*para" + j + " = " + similarity[j]);
            
        }
        
    }
    
    private Double calculateSD(Double mean) {
        Double sd = 0.0;
        for(Double sim : similarity) {
            sd += Math.pow(sim - mean, 2);
        }
        sd = sd/similarity.length;
        sd = Math.sqrt(sd);
        return sd;
    }
    
    private Double calculateSD(Double mean, RealVector vec) {
        double stdDev = 0.0;
        for (int i = 0; i < cols; i++) {
            stdDev += Math.pow(vec.getEntry(i) - mean, 2);
        }
        stdDev = stdDev/rows;
        stdDev = Math.sqrt(stdDev);
        return stdDev;
        
    }
    
    //mean normalize and return mean of each vector
    private double[] meanNormalize() {
//        int rows = docMatrix.getRowDimension();
//        int cols = docMatrix.getColumnDimension();
        docMeanMatrix = MatrixUtils.createRealMatrix(rows, cols);
        double[] mean = new double[rows];
        for (int i = 0; i < rows; i++) {
            RealVector vec = docMatrix.getRowVector(i);
            double vecMean = 0.0;
            for (int j = 0; j < cols; j++) {
                vecMean += vec.getEntry(j);
            }
            
            vecMean /= cols;
            mean[i] = vecMean;
            for (int j = 0; j < cols; j++) {
                double tmp = vec.getEntry(j);
                vec.setEntry(j, tmp-vecMean);
            }
            
            docMeanMatrix.setRowVector(i, vec);
        }
        
        return mean;
    }

    private RealMatrix scaleData(double[] mean) {
        
        RealMatrix ppMatrix = MatrixUtils.createRealMatrix(rows, cols);
        for (int i = 0; i < rows; i++) {
            RealVector vec = docMeanMatrix.getRowVector(i);
            double stdDev = calculateSD(mean[i], vec);
            
            for (int j = 0; j < cols; j++) {
                double tmp = vec.getEntry(j);
                vec.setEntry(j, (tmp-mean[i])/stdDev);                
            }
//            System.out.println(vec.toString());
            ppMatrix.setRowVector(i, vec);
        }
     
        return ppMatrix;
        
    }
    
    public List<Integer> processDataold() {
        List<Integer> outliers = new ArrayList<Integer>();
        //calculate norm vectors
        for(Double[] vector: vectors) {
            Double[] normVector;
            normVector = VectorUtils.normalizeVector(vector);
            normVectors.add(normVector);
        }
        
        //find mean of norm vectors
        meanVector = VectorUtils.meanVector(normVectors);
        
        System.out.print("mean vector = [");
        for (int i = 0; i < meanVector.length; i++) {
            System.out.print(meanVector[i] + ", ");
        }
        System.out.println("]");
        //calculate cosine similarity
        int numVectors = normVectors.size();
        Double meanSim = 0.0;
        
        similarity = new Double[numVectors];
        for (int i = 0; i < numVectors; i++) {
            similarity[i] = CosineSimilarity.similarity(meanVector, normVectors.get(i));            
            meanSim += similarity[i];
            System.out.println("para" + i + " = " + similarity[i]);
        }
        
        
        //calculate mean similarity and Standard Deviation
        meanSim = meanSim / numVectors;
        System.out.println("mean similarity = " + meanSim);
        Double sd = calculateSD(meanSim);
        
        System.out.println("SD = " + sd);
        double threshold = meanSim - (1 * sd);
        System.out.println("threshold = " + threshold);
        for (int i = 0; i < numVectors; i++) {
            
            if(similarity[i] < threshold) {
                System.out.println("Outlier para" + i);
                outliers.add(i);
            }
        }
        return outliers;
    }    
        
}
