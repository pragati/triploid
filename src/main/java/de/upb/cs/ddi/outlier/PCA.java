/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.outlier;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularValueDecomposition;

/**
 *
 * @author lk
 */
public class PCA {
    
    RealMatrix matrix;
    RealMatrix u;
    RealMatrix uT;
    RealMatrix s;
    RealMatrix vT;
    
    public PCA(RealMatrix matrix) {
        this.matrix = matrix;
//        RealMatrix mat = matrix.getSubMatrix(0, 31, startColumn, endColumn);
//        double[][] matrixData = { {1d,1d,0d, 2d}, {1d,1d,2d,0d}, {2d,0d,1d,1d}};
//        matrix = MatrixUtils.createRealMatrix(matrixData);

        SingularValueDecomposition svd = new SingularValueDecomposition(matrix);
//        svd.g
        u = svd.getU();
        uT = svd.getUT();
        s = svd.getS();
        vT = svd.getVT();
        System.out.println("Rank: "+svd.getRank());
//        svd.

        System.out.println("U" + u.getRowDimension() + "x" + u.getColumnDimension());
        System.out.println(u.toString());
        System.out.println("UT" + uT.getRowDimension() + "x" + uT.getColumnDimension());
        System.out.println(uT.toString());
        System.out.println("S" + s.getRowDimension() + "x" + s.getColumnDimension());
        System.out.println(s.toString());
        System.out.println("v" + vT.getRowDimension() + "x" + vT.getColumnDimension());
        System.out.println(vT.toString());
        
        double[] singularValues = svd.getSingularValues();
        double tmp = 0.0;
        for (int i = 0; i < singularValues.length; i++) {
            tmp += singularValues[i];
        }
        
        System.out.println("energy: " + tmp*0.8);
        reduceDimentionality(2);
        
    }
    
    public void reduceDimentionality(int num) {
        
        RealMatrix ured = u.getSubMatrix(0, u.getRowDimension()-1, 0, num-1);
        RealMatrix sred = s.getSubMatrix(0, num-1, 0, num-1);
        RealMatrix vred = vT.getSubMatrix(0, num-1, 0, vT.getColumnDimension()-1);
        
//        System.out.println("matrix" + matrix.getRowDimension() + "x" + matrix.getColumnDimension());
//        System.out.println("U" + ured.getRowDimension() + "x" + ured.getColumnDimension());
        
//        RealMatrix matrixT = matrix.transpose();

        RealMatrix newMatrix = ured.multiply(sred);
        newMatrix = newMatrix.multiply(vred);
//        System.out.println(newMatrix.toString());
        
        
//        RealMatrix vred2 = v.getSubMatrix(0, v.getRowDimension()-1, 0, num-1);
//        RealMatrix newMatrix2 = matrix.multiply(vred2);
//        System.out.println(newMatrix2.toString());
        
        
    }
    
}
