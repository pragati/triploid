/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.outlier;

import java.util.List;

/**
 *
 * @author lk
 */
public class VectorUtils {
    public VectorUtils() {}
    
    public static Double[] normalizeVector(Double[] vector) {
        double magnitude = 0.0;
//        double[] normVector = new double[vector.length];
        for(double entry : vector) {
            magnitude += entry * entry;
        }
        
        magnitude = Math.sqrt(magnitude);
//        System.out.println(magnitude);
        
        for (int i = 0; i < vector.length; i++) {
            vector[i] = vector[i] / magnitude;
        }
        
        return vector;
    }
    
    public static Double[] meanVector(List<Double[]> vectors) {
        Double[] tmp = vectors.get(0);
        int vecSize = tmp.length;
        Double[] meanVector = new Double[vecSize];
        
        for(Double[] vector : vectors) {             
            for (int i = 0; i < vecSize; i++) {
                if(meanVector[i] == null){
                    meanVector[i] = vector[i];
                }else{
                    meanVector[i] += vector[i];
                }
            }
        }
        
        for (int i = 0; i < vecSize; i++) {
            meanVector[i] /= vecSize;
        }
//        System.out.println(meanVector);
        return meanVector;
    }
}
