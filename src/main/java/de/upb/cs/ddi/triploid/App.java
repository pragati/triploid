package de.upb.cs.ddi.triploid;

import de.upb.cs.ddi.documentConverter.FileParser;
import de.upb.cs.ddi.outlier.Outlier;
import de.upb.cs.ddi.tools.misc.corruptsentenceremoval.CorruptSentenceRemovalImpl;
import de.upb.cs.ddi.tools.misc.corrupttokens.CorruptTokenRemovalImpl;
import de.upb.cs.ddi.tools.misc.numberremoval.NumberRemovalImpl;
import de.upb.cs.ddi.tools.misc.symbolremoval.SymbolRemovalImpl;
import de.upb.cs.ddi.tools.postagger.POSTaggerImpl;
import de.upb.cs.ddi.tools.postagger.exceptions.LanguageException;
import de.upb.cs.ddi.tools.utils.RemoveList;
import de.upb.cs.ddi.tools.utils.SubstitutionList;
import de.upb.cs.ddi.tools.utils.exceptions.CouldNotExtractException;
import de.upb.cs.ddi.tools.utils.exceptions.EmptyPDFException;
import de.upb.cs.ddi.tools.utils.exceptions.IListException;
import de.upb.cs.ddi.tools.utils.exceptions.TooManyControlCharacters;
import de.upb.cs.ddi.tools.utils.interfaces.api.NumberRemoval;
import de.upb.cs.ddi.tools.utils.interfaces.api.SymbolRemoval;
import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.RemoveListInt;
import de.upb.cs.ddi.triploid.stylometery.Stylometery;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.TooManyListenersException;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.json.JSONException;

//import org.apache.
public class App {

    public static void main(String[] args) throws FileNotFoundException, IOException, TooManyListenersException, CloneNotSupportedException, TooManyControlCharacters, CouldNotExtractException, EmptyPDFException, LanguageException, JSONException, IListException {
        App instance = new App();
        
//        File dir = new File("/users/lk/Thesis/2008");
//        FileFilter fileFilter = new WildcardFileFilter("*cleaned.txt");
//        File[] files = dir.listFiles(fileFilter);
//    for (int i = 0; i < files.length; i++) {
//           System.out.println(files[i].getName());

        String fileName = "/Users/lk/Thesis/2008/2008_fulltext-25.pdf.pdfbox.cleaned.txt";
        FileParser fp = new FileParser(fileName);
//        FileParser fp = new FileParser(files[i]);
        DocumentInt doc = fp.ParseFile();

//        System.out.println(doc.toString());
//        System.out.println("--------------------------------------------------------------------------------");
        NumberRemoval numberRemoval = new NumberRemovalImpl();
        SymbolRemoval symbolRemoval = new SymbolRemovalImpl();
        
        POSTaggerImpl posTagger = new POSTaggerImpl(POSTaggerImpl.Languages.en);
//
        doc = posTagger.computeSentences(doc);
        doc = posTagger.computeTokens(doc);
        
        SubstitutionList numList = numberRemoval.substituteNumbers(doc);        
        RemoveListInt numremoveList = numberRemoval.removeNumbers(doc);
        RemoveList symbolList = symbolRemoval.removeSymbols(doc);
        RemoveList corruptTokenList = new CorruptTokenRemovalImpl().removeCorruptToken(doc, symbolList, numList, null);
        RemoveList corruptSentList = new CorruptSentenceRemovalImpl().removeCorruptSentences(doc, symbolList, corruptTokenList, numList, null);        

        doc = numremoveList.processDocument(doc);
        doc = symbolList.processDocument(doc);
        
        Stylometery style = new Stylometery();
        List<Double[]> docVector = style.analyzeText(doc);

//        System.out.println(docVector.size());
        
        Outlier outlier = new Outlier(docVector);
        outlier.preProcessData();
//    }   
        
        
       
        
    }

//    public void processPDF() {
//        InputStream is;
////        PDFTextParser pdfParser;
//        List<Double[]> docVector = new ArrayList<Double[]>();
//        List<String> doc = new ArrayList<String>();
//        String fullText = null;
//        try {
//            WordFreq wordfreq = new WordFreq();
////            pdfParser = new PDFTextParser();
////            String text = pdfParser.extractPDFText("/Users/lk/Documents/test.pdf");
//            File fileDir = new File("/Users/lk/Documents/text4.txt");
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));
//
//            StringBuilder sb = new StringBuilder();
//            String line = null;
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//                sb.append(" ");
//            }
//            fullText = sb.toString();
//            br.close();
//
////            System.out.println(text);
//
//            String sentences[] = wordfreq.findSentences(fullText);
//            System.out.println("total sentences : " + sentences.length);
//
//            double start = 0;
//            double end = TriploidConstants.SENTENCE_WINDOW - 1;
//
//            while (end < sentences.length) {
//
//                StringBuilder para = new StringBuilder();
//                for (int i = (int) start; i <= (int) end; i++) {
//                    para.append(sentences[i]);
//                }
//
//                doc.add(para.toString());
//
//                Stylometery stylo = new Stylometery();
//                System.out.println(doc.size());
//                Double[] vector = stylo.analyzeText(para.toString());
//                docVector.add(vector);
//
//                start = end + 1;
//                end += TriploidConstants.SENTENCE_WINDOW;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        Outlier outlier = new Outlier(docVector);
////        List<Integer> outliers = outlier.processData();
//        outlier.preProcessData();
////        for(Integer para : outliers) {
////            System.out.println(doc.get(para));
////        }
//
//
////        System.out.println(fullText.substring(50766, 50766+519));
////        System.out.println(fullText.substring(54844, 54844+808));
//
//        int count = 0;
//        for (int i = 0; i < doc.size(); i++) {
//            System.out.println(i + " - " + count);
//            count += doc.get(i).length();
////            doc.get(i);
//        }
//
////        System.out.println(fullText.substring(54844, 54844+808));
//        SubstitutionList posList = posTagger.computePOS(doc);
//
//        String json = doc.toJSON();

//        doc = posList.processDocument(doc);
//        json += "\n"+doc.toJSON();
//        File file = new File("/Users/lk/Documents/jsonout.txt");
//        if (!file.exists()) {
//            file.createNewFile();
//        }
//
//        FileWriter fw = new FileWriter(file.getAbsoluteFile());
//        BufferedWriter bw = new BufferedWriter(fw);
//        bw.write(json);
//        bw.close();



//        PDFTextParser pdfParser = new PDFTextParser();
//        pdfParser.testStripper("/Users/lk/Documents/test.pdf");

//        CMUdict cmu = new CMUdict();
//        cmu.processFile();
    
    
//    }

    public void testas() {
//        RealMatrix m = MatrixUtils.createRealMatrix(10, 12);
//        m.
//                
    }
}
