package de.upb.cs.ddi.triploid.stylometery;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import com.mysql.jdbc.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author pragatisureka
 */
public class CMUdict {

    private static String DICT_PATH = "src/main/resources/cmudict/cmudict.0.7a";
    private FileReader fileReader;
    private static Connection conn = null;

    public CMUdict() throws FileNotFoundException, IOException {
        this(new FileReader(DICT_PATH));
    }

    public CMUdict(String dictPath) throws FileNotFoundException, IOException {
        this(new FileReader(dictPath));
    }

    public CMUdict(FileReader fileReader) throws IOException {
        this.fileReader = fileReader;

    }

    public void processFile() throws IOException {
        String line;
        int lineNumber = 0;
        String lastWord = "", currentWord = "";
        int lastSyllableCount = 0, curSyllableCount = 0;
//        final Pattern cmuPattern = Pattern.compile("(^[A-Z-]+'?[A-Z]?)(\\(\\d\\))?\\s(.*)");
        final Pattern cmuPattern = Pattern.compile("(.+)\\(\\d\\)$");
        final BufferedReader bufRead = new BufferedReader(this.fileReader);
        SyllableCounter sylCounter = new SyllableCounter();

        while ((line = bufRead.readLine()) != null) {
            lineNumber++;
            
            currentWord = line.substring(0, line.indexOf(" "));
            String phoneticString = line.substring(line.indexOf(" "));

            Matcher m = cmuPattern.matcher(currentWord);
            if(m.find()){
                currentWord = m.group(1);
            }
            
            
            if (currentWord == null && phoneticString == null && "".equals(currentWord) && "".equals(phoneticString)) {
                System.out.println("Error on line " + lineNumber);
            } else {
                curSyllableCount = findSyllables(phoneticString);

                //check if current word is same as last word, if yes then save higher syllable count.                    
                if (lastWord.equals(currentWord)) {
                    if(curSyllableCount > lastSyllableCount) {
                        updateWord(currentWord, curSyllableCount);
                        System.out.println("Found duplicate: " + lineNumber);
                        lastSyllableCount = curSyllableCount;
                    }
                }else{
                    sylCounter.setWord(currentWord);
                    sylCounter.processWord();
                    lastWord = currentWord;
                    lastSyllableCount = curSyllableCount;
                    insertWord(currentWord, curSyllableCount, sylCounter.getSyllableCount());
                }
            }
        }

        bufRead.close();
    }

    private void insertWord(String word, int sylCount, int xcount) {
        Connection con = getMySQLConnection();
        PreparedStatement stmt;
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO syllables (word, syllables, count1) VALUES(?,?,?)");
            stmt.setString(1, word);
            stmt.setInt(2, sylCount);
            stmt.setInt(3, xcount);
            stmt.execute();
            stmt.close();
        } catch (SQLException sqe) {
            sqe.printStackTrace();
            
        }finally {
        }
    }
    private void updateWord(String word, int sylCount) {
        Connection con = getMySQLConnection();
        PreparedStatement stmt;
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE syllables SET syllables=? WHERE word = ?");
            stmt.setString(2, word);
            stmt.setInt(1, sylCount);
            stmt.execute();
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
    }

    private int findSyllables(String pronunciation) {
        int syllableCount = 0;
//        System.out.println(pronunciation);
        for (int i = 0; i < pronunciation.length(); i++) {
            if (Character.isDigit(pronunciation.charAt(i))) {
                syllableCount++;
            }
        }

        return syllableCount;
    }

    public static Connection getMySQLConnection() {
        if(CMUdict.conn != null){
            return CMUdict.conn;
        }
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/thesis?characterEncoding=UTF-8", "root", "root");
            CMUdict.conn = con;
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();

        } catch (SQLException sqe) {
            sqe.printStackTrace();

        }
        return CMUdict.conn;
    }
}
