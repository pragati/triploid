package de.upb.cs.ddi.triploid.stylometery;

/**
 * Calculates the Flesch-Kincaid Grade Level
 * 
 * Calculated as => (0.39 x ASL) + (11.8 x ASW) - 15.59 
 * ASL = average sentence length
 * ASW = average syllables per word
 * 
 * @author lk
 */
public class FleschKincaidGrade {
    
    public FleschKincaidGrade() {
        
    }
    
    public static double calculateGrade(double totalSentences, double totalWords, double totalSyllables) {
        
//        System.out.println("total sentences : " + totalSentences);
//        System.out.println("total words : " + totalWords);
//        System.out.println("total syllables : " + totalSyllables);
        
        double gradeLevel = 0.0;
        if(totalSentences > 0 && totalWords > 0 && totalSyllables > 0){
            gradeLevel = 0.39 * (totalWords / totalSentences) + 11.8 * (totalSyllables / totalWords) - 15.59;
        }
        
        return gradeLevel;
    } 
    
    
}
