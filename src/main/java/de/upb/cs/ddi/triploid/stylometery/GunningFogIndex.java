package de.upb.cs.ddi.triploid.stylometery;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

/**
 * Calculates Gunning Fog index
 *
 * Calculated as => 0.4 (ASL + PCW) 
 * ASL = Average sentence length 
 * PCW = Percentage of complex words
 *
 * PCW is calculated as Count the number of words of three or more syllables
 * that are NOT (i) proper nouns, (ii) combinations of easy words or hyphenated
 * words, or (iii) two-syllable verbs made into three with -es and -ed endings.
 *
 * @author lk
 */
public class GunningFogIndex {

    public GunningFogIndex() {
    }

    public double calculateIndex(double totalSentences, double totalWords, HashMap<String, Integer> syllableMap) {

        double fogIndex = 0.0;

        if (totalSentences > 0 && totalWords > 0) {

            double complexWords = calculateComplexWords(syllableMap);

            fogIndex = 0.4 * ((totalWords / totalSentences) + 100 * (complexWords / totalWords));
        }

        return fogIndex;
    }

    //@lk make it better at finding complex words
    private double calculateComplexWords(HashMap<String, Integer> syllableMap) {
        double complexWords = 0.0;

        for (Map.Entry<String, Integer> entry : syllableMap.entrySet()) {
            int value = entry.getValue();
            if (value > 2) {
                String key = entry.getKey();
                if (value == 3) {
                    if (key.endsWith("ed") || key.endsWith("es") || key.endsWith("es")) {
                        //do nothing
                    } else {
//                        System.out.println(key);
                        complexWords++;
                    }
                } else {
//                    System.out.println(key);
                    complexWords++;
                }
            }
        }

        return complexWords;
    }

//    private void findProperNouns(HashMap<String, Integer> syllableMap) throws IOException {
    private void findProperNouns(String[] sentences) throws IOException {
        InputStream modelIn = null;
        TokenNameFinderModel model;

        modelIn = this.getClass().getClassLoader().getResourceAsStream("en-ner-person.bin");
        model = new TokenNameFinderModel(modelIn);
        NameFinderME nameFinder = new NameFinderME(model);
        Span[] nounSpan = null;
        for (String sentence : sentences) {
//            System.out.println(sentence);
            nounSpan = nameFinder.find(sentences);
            for (Span spn : nounSpan) {
//                System.out.println(spn.toString());
            }
        }

        nameFinder.clearAdaptiveData();
        modelIn.close();

    }
}
