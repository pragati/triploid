/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.triploid.stylometery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculates Honore's R measure
 *
 * Calculated as => (100log N) / 1-(v1/v)
 *
 * N = total number of words 
 * v1 = count of hapax legomena 
 * v = number of different words
 *
 * @author lk
 */
public class HonoresRmeasure {

    public HonoresRmeasure() {
    }

    public double calculateRmeasure(List<String> words) {
//        double hapexCount = hapexLegomenaCount(words);
//        double uniqueWords = uniqueWords(words);
        //0 has hapex, 1 has unique
        
        double[] count = hapexLegomenaAndUniqueCount(words);

        double totalWords = Double.valueOf(words.size());

//        System.out.println("======");
//        System.out.println(totalWords);
//        System.out.println(count[0]);
//        System.out.println(count[1]);
//        System.out.println("======");
        double rMeasure = 0.0;
        if((count[0] == count[1])){
            rMeasure = (100 * Math.log10(totalWords));
        } else {
            rMeasure = (100 * Math.log10(totalWords)) / (1 - (count[0] / count[1]));
        }
        

        return rMeasure;

    }

    private double[] hapexLegomenaAndUniqueCount(List<String> words) {
        double hapexCount = 0;
        double[] count = new double[2];
        Map<String, Integer> freqMap = new HashMap<String, Integer>();

        for (String word : words) {
            Integer freq = freqMap.get(word);
            if (freq == null) {
                freqMap.put(word, 1);
            } else {
                freqMap.put(word, freq + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : freqMap.entrySet()) {
            int value = entry.getValue();
            if(value == 1) {
                hapexCount++;
            }
        }
        
        count[0] = hapexCount;
        count[1] = freqMap.size();
        return count;
    }

    private double uniqueWords(List<String> words) {
        double uniqueWords = 0;

        return uniqueWords;
    }
}
