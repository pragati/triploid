/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.triploid.stylometery;

import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import de.upb.cs.ddi.triploid.TriploidConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import org.json.JSONException;

/**
 *
 * @author lk
 */
public class StyleUtils {
    
    public static NavigableMap<Integer, String> getNextParagraph(DocumentInt doc, int offset) {
        NavigableSet<Integer> sentences = doc.getSentences();
//        System.out.println(doc.toString());
//        System.out.println(offset);
        int newOffset = offset = sentences.higher(offset);
        
        for (int i = 0; i < TriploidConstants.SENTENCE_WINDOW; i++) {
            if(sentences.higher(newOffset) != null) {
                newOffset = sentences.higher(newOffset);
            } else {
                break;
            }
        }
        return doc.getChunks().subMap(offset, true, newOffset, false);
    }

    public static List<String> paraToList(NavigableMap<Integer, String> paragraph) {
        List<String> tokens = new ArrayList<String>();
        for (Map.Entry<Integer, String> entry : paragraph.entrySet()) { 
            tokens.add(entry.getValue());
        }
        return tokens;
    }
    
}
