/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.triploid.stylometery;

import de.upb.cs.ddi.tools.utils.Document;
import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import de.upb.cs.ddi.triploid.TriploidConstants;
import de.upb.cs.ddi.triploid.utils.WordFreq;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import org.json.JSONException;

/**
 *
 * @author lk
 */
public class Stylometery {

    private static double SENTENCE_WINDOW = 6.0;
    
    public Stylometery() {
    }

    public List<Double[]> analyzeText(DocumentInt doc) throws JSONException, IOException {
        List<Double[]> docVector = new ArrayList<Double[]>();
        int offset = -1;
        int numSents = doc.getSentences().size();
//        System.out.println(numSents);
//        System.out.println(doc.toJSON());
        int paracount = 0;
        while(numSents > 0) {
            NavigableMap<Integer, String> paragraph = StyleUtils.getNextParagraph(doc, offset);
            if(!paragraph.isEmpty()) {
                offset = paragraph.lastKey();
            }
//            System.out.println("Para " + paracount);
//            System.out.println(doc.mapToString(paragraph));
            Double vec[] = processParagraph(paragraph);
            docVector.add(vec);
            
            numSents -= TriploidConstants.SENTENCE_WINDOW;
            paracount++;
        }
        
        return docVector;

    }
    
    public Double[] processParagraph(NavigableMap<Integer, String> paragraph) throws IOException {
        
        List<Double> vector = new ArrayList<Double>();
        
        List<String> tokens = StyleUtils.paraToList(paragraph);

        SyllableCounter sCounter = new SyllableCounter();
        double totalSyllaCount = 0;
        HashMap<String, Integer> syllableMap = new HashMap<String, Integer>();

        for (String token : tokens) {
            int syllaCount = sCounter.countSyllables(token);
            syllableMap.put(token, syllaCount);
            totalSyllaCount += syllaCount;
        }
        
        double totalWordLength = 0.0;
        for (String token : tokens) {
            totalWordLength += token.length();
        }

        vector.add(Double.valueOf(tokens.size() / SENTENCE_WINDOW));
        vector.add(totalSyllaCount / Double.valueOf(tokens.size()));
        vector.add(totalWordLength / tokens.size());
        
        double fkgrade = FleschKincaidGrade.calculateGrade(SENTENCE_WINDOW, Double.valueOf(tokens.size()), totalSyllaCount);
        vector.add(fkgrade);
        
        GunningFogIndex gf = new GunningFogIndex();
        double gfIndex = gf.calculateIndex(SENTENCE_WINDOW, Double.valueOf(tokens.size()), syllableMap);
        vector.add(gfIndex);

        HonoresRmeasure hrm = new HonoresRmeasure();
        double hrMeasure = hrm.calculateRmeasure(tokens);
        vector.add(hrMeasure);

        YuleKMeasure ykm = new YuleKMeasure();
        double ykmeasure = ykm.calculateMesure(tokens);
        vector.add(ykmeasure);
        
//        System.out.println("===================================");
//        System.out.println("Total Syllables : " + totalSyllaCount);
//        System.out.println("ASL = " + tokens.size() / SENTENCE_WINDOW);
//        System.out.println("ASW = " + totalSyllaCount / Double.valueOf(tokens.size()));
//        System.out.println("AWL = " + totalWordLength / tokens.size());
//        System.out.println("Flesch Kincaid Grade = " + fkgrade);
//        System.out.println("Gunning fog index = " + gfIndex);
//        System.out.println("Honore R measure = " + hrMeasure);
//        System.out.println("Yule's K measure = " + ykmeasure);
//        System.out.println("===================================");
//        System.out.println(vector.toString());
        return vector.toArray(new Double[vector.size()]);
    }
}
