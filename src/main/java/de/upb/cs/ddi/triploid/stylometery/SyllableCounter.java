package de.upb.cs.ddi.triploid.stylometery;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 *
 *
 * @author pragatisureka
 */
public class SyllableCounter {

    private String word;
    private int syllableCount;
    private static Connection conn = null;
    //lists from ruby docs http://rubydoc.info/github/vshulman/RubyRhymes/Phrase/Pronunciations
    private final String[] subSyllables = {"cial", "tia", "cius", "cious", "uiet", "gious", "geous", "priest",
        "giu", "dge", "ion", "iou", "sia$", ".ched$",
        ".aged$", ".aled$", ".ales$", ".ashed$", ".aped$", ".athes$",
        ".esques$",
        ".iped$",
        ".ished$",
        ".osques$",
        ".pped$", ".ssed$",
        ".ules$", ".uled$",
        ".ushed$", ".ved$", ".wes$", ".wed$",
        ".red$", ".ily$", ".ely$", ".des$", ".gged$",
        ".kes$", ".ced$", ".ked$", ".med$", ".mes$", ".ned$", ".sed$", ".rles$",
        ".nes$", ".pes$", ".tes$", ".res$", ".ves$"};
    private final String[] addSyllables = {"ia", "riet", "dien", "ien", "iet", "iu", "iest", "io", "ii", "ily", ".oala$",
        ".iara$", ".ying$", ".earest", ".arer", ".aress", ".eation$", "[aeiouym]bl$", "..ie$",
        "[aeiou]{3}", "^mc", "ism", "^mc", "asm", "([^aeiouy])\1l$", "[^l]lien", "^coa[dglx].",
        "[^gq]ua[^auieo]", "dnt$"};
    private List<Pattern> subSylPattern = new ArrayList<Pattern>();
    private List<Pattern> addSylPattern = new ArrayList<Pattern>();

//    private List<Pattern> subSylPattern;
    public SyllableCounter() {
        this.syllableCount = 0;
        initializePatterns();
    }

    public SyllableCounter(String word) {
        this.word = word;
        this.syllableCount = 0;
        initializePatterns();
    }

    private void initializePatterns() {
        // subtract syllable patterns
        for (String sylPattern : this.subSyllables) {
            this.subSylPattern.add(Pattern.compile(sylPattern));
        }
        // add syllable patterns
        for (String sylPattern : this.addSyllables) {
            this.addSylPattern.add(Pattern.compile(sylPattern));
        }
    }

    
    //@lk use only one mysql connection for all words...
    public int countSyllables(String word) {
//        Connection con = getMySQLConnection();
        Connection con = null;
        if(con != null){
            PreparedStatement stmt = null;
            try {
                stmt = (PreparedStatement) con.prepareStatement("SELECT syllables FROM syllables WHERE word=?");
                stmt.setString(1, word);
                stmt.execute();
                ResultSet rs = stmt.getResultSet();

                if (rs != null && rs.next()) {
                    syllableCount = rs.getInt(1);
    //            System.out.println(word + " = " + syllableCount);
                } else {
                    setWord(word);
                    processWord();
    //            System.out.println(word + " => " + syllableCount);
                }

                stmt.close();

            } catch (SQLException sqe) {
                sqe.printStackTrace();

            }            
        }else {
            setWord(word);
            processWord();
        }

        return syllableCount;
    }

    public void processWord() {
        String currentWord = word.toLowerCase();

        //check for numbers and special characters
        int numVowels = 0;
        int sylCount = 0;
        int len = currentWord.length();

        //count number of vowels
        //y is considered because it sounds like "i-e"
        boolean lastVowel = false;
        for (int i = 0; i < len; i++) {
            if ('a' == currentWord.charAt(i) || 'e' == currentWord.charAt(i) || 'i' == currentWord.charAt(i) || 'o' == currentWord.charAt(i) || 'u' == currentWord.charAt(i) || 'y' == currentWord.charAt(i)) {
                //skip diphthongs
                if (!lastVowel) {
                    numVowels++;
                    lastVowel = true;
                }
            } else {
                lastVowel = false;
            }
        }

        //check for last "e" its mostly silent.
        if (currentWord.charAt(len - 1) == 'e') {
            numVowels--;
        }


        int subSyllable = 0;
        for (Pattern p : subSylPattern) {
            Matcher m = p.matcher(currentWord);
            if (m.find()) {
//                System.out.println("sub: " + p.toString());
                subSyllable++;
            }
        }

        int addSyllable = 0;
        for (Pattern p : addSylPattern) {
            Matcher m = p.matcher(currentWord);
            if (m.find()) {
//                System.out.println("add: " + p.toString());
                addSyllable++;
            }
        }

        sylCount = numVowels - subSyllable + addSyllable;

        if (sylCount == 0) {
            //if sylCount is 0 that means there is atleast one syllable
            sylCount = 1;
        }

        this.syllableCount = sylCount;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * @return the syllableCount
     */
    public int getSyllableCount() {
        return syllableCount;
    }

    public static Connection getMySQLConnection() {
        if (SyllableCounter.conn != null) {
            return SyllableCounter.conn;
        }
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/thesis?characterEncoding=UTF-8", "root", "root");
            SyllableCounter.conn = con;
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
//            System.out.println("No mysql");
        } catch (SQLException sqe) {
//            sqe.printStackTrace();
//            System.out.println("No mysql con");
        }
        return SyllableCounter.conn;
    }
}
