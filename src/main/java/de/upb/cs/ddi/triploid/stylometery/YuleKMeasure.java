/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.triploid.stylometery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculates Yule's K measure
 * 
 * Calculated as => 10000 x ((-1/N)+sum(vi(i/N)2)) OR,
 *  1000 x (M-N/N x N)
 * 
 * vi = number of different words that occur i times
 * N = number of words 
 * 
 * @author lk
 */
public class YuleKMeasure {
    
    public YuleKMeasure() {
    }
    
    public double calculateMesure(List<String> words) {
        double ykMeasure = 0.0;
        double numWords = words.size();
        
//        System.out.println(words.size());
        Map<Integer, Integer> countMap= processTokens(words);
//        System.out.println(countMap.toString());
        
        double sum = calculateSum(countMap, numWords);
        
//        double sum1 = calculateM(countMap, numWords);
        
        ykMeasure = 10000 * (sum - (1/numWords));
        
//        System.out.println(ykMeasure);
//        System.out.println(10000 * sum1);
        return ykMeasure;
    }
    
    private Map<Integer, Integer> processTokens(List<String> words) {
        Map<String, Integer> wordFreq= new HashMap<String, Integer>();
        
        for (String word : words) {
            Integer freq = wordFreq.get(word);
            if (freq == null) {
                wordFreq.put(word, 1);
            } else {
                wordFreq.put(word, freq + 1);
            }
        }
        
        Map<Integer, Integer> countMap= new HashMap<Integer, Integer>();
        for (Map.Entry<String, Integer> entry : wordFreq.entrySet()) {
            int value = entry.getValue();
            String key = entry.getKey();
            
            Integer numWords = countMap.get(value);
            
            if(numWords == null) {
                countMap.put(value, 1);
            }else{
                countMap.put(value, numWords + 1);                
            }
        }
        
        
        return countMap;
    }
    
    private double calculateSum(Map<Integer, Integer> countMap, double totalWords) {
        double sum = 0.0;
        
        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            Integer numWords = entry.getValue();
            Integer count = entry.getKey();
            
            sum += numWords * (count/totalWords) * (count/totalWords);
        }
        
        return sum;
    }
    
    
    private double calculateM(Map<Integer, Integer> countMap, double totalWords) {
        double m = 0.0;
        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            Integer numWords = entry.getValue();
            Integer count = entry.getKey();
            
            m += numWords * count * count;            
        }
        
        m = (m-totalWords)/(totalWords * totalWords);
        return m;
        
    }
    
}
