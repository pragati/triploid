/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.triploid.utils;

import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import de.upb.cs.ddi.triploid.TriploidConstants;
import java.util.NavigableMap;
import java.util.NavigableSet;

/**
 *
 * @author lk
 */
public class DocUtils {
    
//    public DocUtils() {}
    
    public static NavigableMap<Integer, String> getNextParagraph(DocumentInt doc, int offset) {
        NavigableSet<Integer> sentences = doc.getSentences();
        int newOffset = sentences.first();
        //can only happen for first sentence
        if(offset < newOffset) {
            offset = newOffset;
        }else {
            newOffset = offset;
        }
        
        for (int i = 0; i < TriploidConstants.SENTENCE_WINDOW; i++) {
            newOffset = sentences.higher(newOffset);
        }
        
        return doc.getChunks().subMap(offset, true, newOffset, false);
    }
    
}
