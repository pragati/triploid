/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.upb.cs.ddi.triploid.utils;

import de.upb.cs.ddi.tools.utils.Document;
import de.upb.cs.ddi.tools.utils.interfaces.datastructurs.DocumentInt;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TooManyListenersException;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 *
 * @author pragatisureka
 */
public class PDFTextParser {
    
    private int numPages;
    
    public String extractPDFText(String fileName) throws IOException, TooManyListenersException, CloneNotSupportedException {
        
        
        PDDocument pddoc = PDDocument.load(fileName);
        DocumentInt doc = new Document();
        
        setNumPages(pddoc.getNumberOfPages());
        StringBuilder sb = new StringBuilder();
        
        PDFTextStripper stripper = new PDFTextStripper();
        for (int i = 1; i <= pddoc.getNumberOfPages(); i++) {
            stripper.setStartPage(i);
            stripper.setEndPage(i);
            doc.addPage();
            String fulltext = stripper.getText(pddoc);
                        
            String[] search = {"\u0000", "\u0001", "\u0002", "\u0003", "\u0004", "\u0005", "\u0006", "\u0007", "\u0008", "\u0009", "\u000B", "\u000C", "\u000E", "\u000F", "\u0010", "\u0011", "\u0012", "\u0013", "\u0014", "\u0015", "\u0016", "\u0017", "\u0018", "\u0019", "\u001A", "\u001B", "\u001C", "\u001D", "\u001E", "\u001F"};
            String[] replace = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
            String fulltext_without_control = StringUtils.replaceEach(fulltext, search, replace);
            float controlcharaterratio = 1 - (((float) fulltext_without_control.length()) / ((float) fulltext.length()));
            if (controlcharaterratio > 0.3) {
                throw new TooManyListenersException("Control character ratio is: " + controlcharaterratio);
            } else {
                addTextToDoc(doc, fulltext_without_control);
            }
            sb.append(fulltext);
        }
        
        
//        doc = TextCleaner.removeHeader(doc);
//        doc = TextCleaner.removeFooter(doc);
        
        pddoc.close();
        return sb.toString();
//        return doc;
    }    
    
    
    public void testStripper(String fileName) throws IOException {
        PDDocument pddoc = PDDocument.load(fileName);
        
        StringBuilder sb = new StringBuilder();
        
        PDFTextStripper stripper = new PDFTextStripper();
        for (int i = 1; i <= pddoc.getNumberOfPages(); i++) {
            stripper.setStartPage(i);
            stripper.setEndPage(i);
            stripper.setParagraphStart("~~~~START-PARA~~~~");
            stripper.setParagraphEnd("~~~~END-PARA~~~~");
            String fulltext = stripper.getText(pddoc);
            System.out.println(fulltext);
//            sb.append(fulltext);
        }        
    }
    
    public static void addTextToDoc(DocumentInt doc, String text) throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(text));
        String line = null;
        LinkedList<String> lines = new LinkedList<String>();
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }

        boolean lastcharacterislinefeed = false;
        if (new Character(text.charAt(text.length() - 1)) == '\n') {
            lastcharacterislinefeed = true;
        }

        if (!lastcharacterislinefeed) {
            String last = lines.getLast().replace("\n", "");
            if (last.charAt(last.length() - 1) != ' ' && last.charAt(last.length() - 1) != '-') {
                last = last + " ";
            }
            lines.removeLast();
            lines.add(last);
        }

        Iterator<String> it = lines.iterator();
        while (it.hasNext()) {
            doc.addLine();
            String linestr = it.next();
            if (linestr.length() != 0 && linestr.charAt(linestr.length() - 1) != ' ' && linestr.charAt(linestr.length() - 1) != '-') {
                linestr = linestr + " ";
            }
            doc.addChunk(linestr);
        }
    }

    /**
     * @return the numPages
     */
    public int getNumPages() {
        return numPages;
    }

    /**
     * @param numPages the numPages to set
     */
    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }
    
}
