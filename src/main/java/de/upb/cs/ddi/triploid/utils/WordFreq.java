package de.upb.cs.ddi.triploid.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

/**
 *
 * @author pragatisureka
 */
public class WordFreq {
    
    
    public String[] findSentences(String text) throws CloneNotSupportedException, FileNotFoundException, IOException {
        InputStream modelIn = null;
        SentenceModel model;

        modelIn = this.getClass().getClassLoader().getResourceAsStream("en-sent.bin");
        model = new SentenceModel(modelIn);
        SentenceDetectorME sentenceDetector = new SentenceDetectorME(model);
        String sentences[] = sentenceDetector.sentDetect(text);
        modelIn.close();
        return sentences;        
    }

    public List<String> tokenizeString(String sent) throws IOException {
        InputStream modelIn = null;
        TokenizerModel model;
        
        modelIn = this.getClass().getClassLoader().getResourceAsStream("en-token.bin");
        model = new TokenizerModel(modelIn);
        Tokenizer tokenizer = new TokenizerME(model);
        sent = sent.replace("'", "");
        String tokens[] = tokenizer.tokenize(sent);
        modelIn.close();        
        return removeSymbols(tokens);
    }
    
    private List<String> removeSymbols(String[] tok) {
        List<String> tokens = new ArrayList<String>();
        if(tok.length > 0) {
            for(String t : tok ) {
                if(t.length() == 1) {
                    char c = t.charAt(0);
                    if(Character.isDigit(c) || Character.isLetter(c)) {
                        tokens.add(t);
                    }
                }else{
                    tokens.add(t);                    
                }                
            }
        }
        
        return tokens;
    }
}
